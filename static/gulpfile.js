const gulp = require('gulp');
const sass = require('gulp-sass');
const cssnano = require('gulp-cssnano');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const eslint = require('gulp-eslint');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const live = require('gulp-livereload');

const packageInfo = require('./package.json')

gulp.task('css', () => {
    gulp.src('src/sass/teatr.scss')
        .pipe(sass())
        // .pipe(autoprefixer({
        //     browsers: [
        //         'IE >= 11',
        //         'Firefox >= 43',
        //         'Chrome >= 40',
        //         'iOS >= 8',
        //         'Safari >= 8',
        //         'Opera >= 36'
        //     ]
        // }))
        .pipe(autoprefixer())
        .pipe(cssnano())
        .pipe(rename({
			basename: packageInfo.name,
			suffix: '.min',
			extname: '.css'
		}))
        .pipe(gulp.dest('public/css/'))
        .pipe(live());
});

gulp.task('js', () => {
    gulp.src(['src/js/*', 'src/js/plugins/**', 'src/js/modules/*'])
        .pipe(concat('teatr.js'), {newLine: ';'})
        .pipe(uglify())
        .pipe(rename({
			basename: packageInfo.name,
			suffix: '.min',
			extname: '.js'
		}))
        .pipe(gulp.dest('public/js/'))
        .pipe(live());
});

gulp.task('eslint', () => {
	const src = [
		'./src/js/**',
		'!./src/js/plugins/**'
	];

	return gulp.src(src)
		.pipe(eslint())
		.pipe(eslint.format());
});

gulp.task('watch', () => {
    live.listen();
	gulp.watch('src/js/**', ['js', 'eslint']);
	gulp.watch('src/sass/**', ['css']);
});

gulp.task('default', ['css', 'js', 'eslint']);
