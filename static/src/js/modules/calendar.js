function Calendar(start, place) {

	this.month = start.getMonth();
    this.year = start.getFullYear();
	this.days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]; // TODO: dorobić luty przestępny
	this.monthNames = ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'];
    this.newYear = false;

	this.firstDayOfMonth = function (month, year) {
        var date = new Date(year, month, 1);
		var day = date.getDay();
		if (day === 0) {
			day = 7;
		}
		return day;
	};

    this.checkYear = function (month) {
        if ((month / 11) > 1){
            this.newYear = true;
        }
    };

	this.renderCalendar = function () {
        var min = 7;
		for (var i = 0; i < 3; i++) {

            var month = (this.month + i) % 12;
            this.checkYear(this.month + i);
            var year = this.year + ((this.newYear) ? 1 : 0);
			var row = new Array(37);
			row.fill('<div></div>');
			var dayInMonth = this.days[month];
			var day = this.firstDayOfMonth(month, year);
            if (day < min){
                min = day;
            }

			for (var j = day; j < dayInMonth + day; j++) {
				row[j - 1] = '<div data-date="' + year + '-' + (month + 1) + '-' + (j - day + 1) + '">' + (j - day + 1) + '</div>';
			}

            var days = $('<div class="calendar__days"></div>');
            days.append(row);
            var rowWrapper = $('<div class="calendar__row"><span class="calendar__monthName">' + this.monthNames[month] + '</span></div>');
            rowWrapper.append(days);
            place.append(rowWrapper);
		}

        var maxWidth = 0;
        place.find('.calendar__monthName').each(function() {
            if ($(this).width() > maxWidth){
                maxWidth = $(this).width();
            }
        });
        place.find('.calendar__monthName').css('width', maxWidth);
        place.find('.calendar__days').css('left', -(min - 1) * 30);
	};

    this.watchDays = function (input) {
        place.find('.calendar__day--withSpectacle').on('click', function() {
            $('.calendar__day--withSpectacle').removeClass('calendar__day--active');
            $(this).addClass('calendar__day--active');
            input.val($(this).data('date')).trigger('change');
        });
    };

    this.renderCalendar();
}
