$(function () {

	var collapsible = $('.filterBox__collapsible');

	$('.filterBox__city').on('click', function () {
		$('#selectedCity').val($(this).data('city'));
		$('.filterBox__distance').removeClass('filterBox__distance--disabled')
		$('#selectedCity').trigger('change');
		collapsible.addClass('filterBox__collapsible--collapsed');
	});

	$('.filterBox__trigger').on('click', function () {
		collapsible.toggleClass('filterBox__collapsible--collapsed');
	});

	var distance50 = $('#distance50');
	var distance100 = $('#distance100');

	distance50.on('change', function () {
		$(this).parent().toggleClass('filterBox__distance--active').removeClass('filterBox__distance--hovered');
		if (distance100.prop('checked')) {
			distance100.prop('checked', false).parent().removeClass('filterBox__distance--active');
		}
	});

	distance100.on('change', function () {
		$(this).parent().toggleClass('filterBox__distance--active');
		if ($(this).prop('checked')) {
			distance50.prop('checked', false).parent().removeClass('filterBox__distance--active').addClass('filterBox__distance--hovered');
		} else {
			distance50.parent().removeClass('filterBox__distance--hovered');
		}
	});
});

function watchInputs(filterBox) {
	filterBox.find('input:not(#filterBoxQuery)').on('change', function () {
		filterResults(filterBox);
	});
	filterBox.find('.filterBox__query').on('keyup', $.debounce(300, function () {
		filterResults(filterBox);
	}));
}

function filterResults(form) {
	console.log(form.serialize());

}

var xdd = new Calendar(new Date(2017, 4, 1), $('#calendar'));

watchInputs($('.filterBox__form'));

$('#filterResultContainer').find('.tile--spectacle')
	.each(function (index, el) {
		$('#calendar').find('div[data-date="' + $(this).data('date') + '"]').addClass('calendar__day--withSpectacle');
	})
	.on('mouseover', function () {
		$($('#calendar').find('div[data-date="' + $(this).data('date') + '"]')).addClass('calendar__day--active')
	})
	.on('mouseout', function () {
		$('#calendar').find('div').removeClass('calendar__day--active')
	});
    
xdd.watchDays($('#selectedDate'));
