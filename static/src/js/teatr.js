var teatr = {};

teatr.closeOnOutsideClick = function (event) {
	if (!$(event.target).closest('.filterBox__collapsible').length) {
		$('.filterBox__collapsible').addClass('filterBox__collapsible--collapsed');
	}
};

$(document).on('click', teatr.closeOnOutsideClick);
